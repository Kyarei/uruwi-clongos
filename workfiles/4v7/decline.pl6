#!/usr/bin/env perl6

use fatal;

=begin comment

Usage: perl6 workfiles/4v7/decline.pl6 <noun> [more principal parts...]

Additional principal parts go in the order A -> G -> L -> I -> S, skipping
any that are not applicable for the noun class.

Needs to be run in the root of the repository in order to access the
dictionary.

TODO: actually add support for specifying principle parts
TODO: update to reflect latest changes (i.e. lenition after þja○-)

=end comment

my $VOWELS = / <[aeiouâêîô]> /;

constant %VOICED_CONSONANTS = %(
  :p<v>, :f<v>, :þ<ð>, :t<d>, :c<g>
);

constant %THEMATIC_VOWEL_DERIVATIVES_V = %(
  "a" => <o e o>,
  "e" => <o i i>,
  "i" => <jo i i>,
  "o" => <o e i>,
  "u" => <u o i>,
  "ja" => <jo je jo>,
  "je" => <jo i i>,
  "jo" => <jo je i>,
);

constant %THEMATIC_VOWEL_DERIVATIVES_AS = %(
  "a" => <a o e e e o e>,
  "e" => <a o e i i i i>,
  "i" => <i jo ja i i i i>,
  "u" => <i u u o e i i>,
  "ja" => <ja jo e je e jo e>,
  "je" => <ja jo e i i i i>,
);

constant %THEMATIC_VOWEL_DERIVATIVES_AN = %(
  "a" => <o e e e o e>,
  "e" => <i i i i i i>,
  "i" => <jo ja i i i i>,
  # o and jo lines are kept for G stem derivation
  "o" => <a>,
  "u" => <u e o e i i>,
  "ja" => <jo e je e jo e>,
  "je" => <i i i i i i>,
  "jo" => <ja>,
);

constant %THEMATIC_VOWEL_DERIVATIVES_IL = %(
  "a" => <a o o e e i o e>,
  "o" => <e o o e jo o o e>,
  "i" => <i jo i i i i i i>,
  "u" => <e e i e o o i e>,
  "ja" => <ja jo i e je i o je>,
  "jo" => <je i i i jo i i je>,
);

constant %DECLENSIONS = %(
  "V" => <
    N0 N0c N1
    N0n N0ŋ N1n
    N0s N0ci N1s
    N2n N2c N3n
    L0s L2sac L1s
    L2sa L2sac L2sta
    L2la L2lac L2lta
    L2na L2nac L1na
    L2ca L2hac L2cta
    L2þa L2þac L3þa
    Sic Sic Sicþi
    Sit Sict0 S2t
  >,
  "AS" => <
    N0s N0c N1
    N0n N0ŋ N1'n
    N0þa N0sci N1þ
    N2'n N2c N3'n
    L0þ0 L0s0'c L1þ
    L0þ0 L0s0 L2s0
    L0ł0 L0ł0'c L2ł0
    L0sn0 L0sn0'c L1'sn0
    L0cþ0 L0cþ0'c L1cþ0
    L2ð0 L2ð0c L3ða
    Sic Sic Sicþi
    Sis Sist0 S2't
  >,
  "AN" => <
    N0ñ N0c N1'ñ
    Nañ0n N0ŋ Nañ1'n
    N0s N0ci N1's
    N2'r N2ri N3'r
    L0s L0saŋ L1's
    L0nsa L0nsac L1'nsa
    L0la L0lac L1'la
    L0na L0nac L1'na
    L0ŋa L0ŋac L1'ŋa
    L2nþa L2nþac L3nþa
    Siŋ Sic Siŋþi
    Sit Siŋto S2't
  >,
  "AR" => <
    N0r N0c N1
    N0n N0ŋ N1'n
    N0s N0ci N1's
    N2'n N2ci N3'n
    L0s L0sac L1's
    L0rsa L0rsac L1'rsa
    L0ra L0rac L1'ra
    L0rna L0rna L1'rna
    L0ca L0cac L1'ca
    L2'rþa L2'rþac L3'rþa
    Sic Sic Sicþi
    Sir Sirc0 S2't
  >,
  "Þ" => <
    N0þ N0c N1þ
    N0n N0ŋ N1n
    N0þas N0cþi N1þas
    N2n N2c N3n
    L0þas L0þac L1þas
    L0þas L0cþas L1þas
    L0rþa L0rþac L2rþa
    L0nþa L0nþac L2nþa
    L0cþa L0cþac L1cþa
    L2þa L2þac L3þa
    Sic Sic Sicþi
    Siþ Sicþ0 S2t
  >,
  "RÞ" => <
    N0rþ N0c N1rþ
    N0n N0ŋ N1n
    N0rþas N0rcþi N1rþas
    N2r N2cir N3r
    L0rþas L0rþac L1rþas
    L0rþas L0rcþas L1rþas
    L0lþa L0lþac L2lþa
    L0nþa L0nþac L2nþa
    L0rcþa L0rcþac L1rcþa
    L2rþa L2rþac L3rþa
    Sic Sic Sircþi
    Sirþ Sircþ0 S2r
  >,
  "IL" => <
    N0l N0c N1'l
    N0*n N0*ŋ N1*n
    N0*s N0lci N1*s
    N3*r N2cir N3*r
    L0*s L0sac L1*s
    L0ła L0łac L1'rła
    L0la L0lac L1'lta
    L0lna L0lna L1'lna
    L0lcþa L0lcþac L1'lcþa
    L2'lþa L2'lþac L3'lþa
    Sic Sic Silcþi
    Sil Silc0 S2'l
  >,
  "OS" => <
    N0s N0c N0r
    N0n N0ŋ N0n0r
    Nas0s Nas0c Nas0r
    N1l Nac1l Ni1l
    L0s L0sac L0r
    L0sa L0sac L0ra
    L0la L0lac L0rla
    L0na L0na L0rna
    cjaI0s cjaI0c cjaI0r
    þjaJ0s þjaJ0c þjaJ0r
    Soc Soc Socþe
    Sot Soct0s S0t
  >,
  "OR" => <
    N0r N0c N0s
    N0n N0ŋ N0n0s
    Nas0r Nas0c Nas0s
    N1l Nac1l Ni1l
    L0s L0sac L0r
    L0ra L1sac L1sa
    L1la L1lac L1lsa
    L0na L0na L0nsa
    cjaI0r cjaI0c cjaI0s
    þjaJ0r þjaJ0c þjaJ0s
    Soc Soc Socþe
    Sot Soct0s S0t
  >,
  "ON" => <
    N0ñ N0c N0r
    Nan0ñ Nan0ŋ Nan0r
    N0s Nas0c Nas0r
    N1l Nac1l Ni1l
    L0s L1sac L0r
    L1sa L1sac L0ra
    L1la L1lac L0rla
    L0na L0na L0rna
    cjaI0ñ cjaI0c cjaI0r
    þjaJ0ñ þjaJ0c þjaJ0r
    Soc Soc Socþe
    Sot Soct0s S0t
  >,
  "EL" => <
    N0l N1c Ni0l
    A0n A1c A1n
    A1s A1ci N1s
    G0l Nac0l G1l
    L1s L1sac L1r
    L0sa L0sac L0rþa
    L0la L0lac L0rla
    L1na L1na L1rna
    cjaI0l cjaI1c cjaI1r
    þjaJ0l þjaJ1c þjaJ1r
    Soc Soc Socþe
    Sot Soct1s S1t
  >,
);

constant @MONOSYLLABIC-OVERRIDES = <<
  "" "" Or
  Ön Os Os
  i ic ir
  a ac o
>>;

constant %LETTERNUMS = %(<<
  c 0 e 1 n 2 ŋ 43 v 3 o 4 s 5
  þ 85 š 94 r 6 l 7 ł 119
  m 32 a 9 f 10 g 11 p 12 t 13 č 222
  î 14 j 110 i 15 d 16 ð 341
  h 17 ħ 18 ê 257 ô 260 â 265 u 19
  '#' 20 + 21 +* 22 @ 23 * 25
>>);

constant $LENITE = set <p t d č c g m f v ð>;

constant %ECLIPSE = %(<
  p m
  t d
  d n
  c g
  g ŋ
  f v
  þ ð
  ł l
  a g e g i g o g u g â g ê g î g ô g
>);

constant $SINGULAR-GENITIVE = 9;
constant $ECLIPSED-IF-MULTISYLLABIC = 10 | 11;

constant \NCASES = 12;
constant \NNUMBERS = 3;

constant @CASE_NAMES = <<
  Nominative Accusative Dative Genitive
  Locative-temporal Ablative Allative Prolative
  Instrumental-comitative Abessive
  "Semblative I" "Semblative II"
>>;

sub extract-marker(Str $s) {
  my $m = $s ~~ /^ ([ '#' | '+' | '+*' | '@' | '*' ]*) ([\w | '·']+) $/;
  die "'$s' is not a valid word" if !$m;
  ~$m[0], ~$m[1];
}

sub transform-themvowel(%tab, Str $thv) {
  my $thv-no-dia = $thv.samemark('a');
  %tab{$thv-no-dia}.map({ .samemark($thv.chomp[* - 1]).subst('ĵ', 'j') });
}

sub toggle-j(Str $s) {
  $s.subst(/(j?) ($VOWELS)/, {
    ~$1 eq 'i' | 'î' ?? ~$1 !!
      ~$0 ?? ~$1 !! 'j' ~ ~$1;
  }, :1st);
}

sub has-pf-onset(Str $s) {
  return $s ~~ / ^ <[ptdcg]> <[fvþðsšhħ]> /;
}

sub lenite(Str $s) {
  return $s if has-pf-onset($s);
  given $s ~~ / ^ (.) (.*) $ / {
    if ~$0 (elem) $LENITE {
      return "$0·$1";
    }
  }
  return $s;
}

sub eclipse(Str $s, Str $m = "") {
  return $m ~ $s if has-pf-onset($s);
  given $s ~~ / ^ (.) (.*) $ / {
    if ~$0 eq 'p' && "eiuêîû".contains((~$1).comb[0]) {
      return "v$m$s";
    }
    my $t = %ECLIPSE{~$0};
    return "$t$m$s" if $t;
  }
  return $m ~ $s;
}

sub is-multisyllabic(Str $s) {
  return $s.comb($VOWELS) >= 2;
}

sub letter-sum(Str $n) {
  [+] $n.comb(/ . | '+*' /).map({%LETTERNUMS{$_}});
}

sub read-dictionary(IO::Handle $fh, Bool $realigned) {
  my %entries;
  my $cur-entry;
  my $lineno = 0;
  for $fh.lines {
    ++$lineno;
    if /^ "#" \s* (.*)/ {
      die "Name redeclared at line $lineno" if $cur-entry<name>.defined;
      $cur-entry<name> = ~$0;
    } elsif /^ ":" \s* (.*)/ {
      die "POS redeclared at line $lineno" if $cur-entry<pos>.defined;
      $cur-entry<pos> = ~$0;
    } elsif /^ "<" \s* (.*)/ {
      $cur-entry<etym> = ~$0;
    } elsif /^ "@" (\w+) \s* (.*)/ {
      $cur-entry<tags>{~$0} = ~$1;
    } elsif $_ {
      if $cur-entry<def>:exists {
        $cur-entry<def> ~= "\n$_";
      } else {
        $cur-entry<def> = $_;
      }
    } elsif $cur-entry {
      if not $cur-entry<name>:exists {
        die "Attempted to push entry without name at line $lineno";
      }
      if not $cur-entry<def>:exists {
        die "Attempted to push entry without definition at line $lineno";
      }
      my $pos = $cur-entry<pos>;
      if $pos ~~ / "n/tr=" (.) ",re=" (.) / {
        $pos = "n" ~ ~($realigned ?? $1 !! $0);
      }
      if $pos eq 'nc' | 'nt' | 'nh' {
        $cur-entry<pos> = $pos;
        %entries{$cur-entry<name>} = $cur-entry;
      }
      $cur-entry = Hash();
    }
  }
  if $cur-entry {
    if not $cur-entry<name>:exists {
      die "Attempted to push entry without name at EOF";
    }
    if $cur-entry<pos> eq 'nc' | 'nt' | 'nh' {
      %entries{$cur-entry<name>} = $cur-entry;
    }
  }
  %entries;
}

sub get-stem-and-suffix(Str $noun, Bool :$stripj = False, Bool :$fatal = True) {
  # Get everything from last vowel on
  my $r = $stripj ?? $VOWELS !! /j? $VOWELS/;
  my @matches = $noun.match($r, :global);
  return ($noun, '') if !@matches && !$fatal;
  die "This isn't Drsk!" if !@matches;
  my $m = @matches[* - 1];
  my $stem = $noun.substr(0, $m.from);
  my $suffix = $noun.substr($m.from);
  $stem, $suffix;
}

sub get-stem-and-suffix-twice(Str $noun, Bool :$stripj = False, Bool :$fatal = True) {
  my ($s1, $f1) = get-stem-and-suffix($noun, :$stripj, :$fatal);
  my ($s2, $f2) = get-stem-and-suffix($s1, :$stripj, :$fatal);
  $s2, $f2 ~ $f1;
}

sub get-suffix-class-and-thematic-vowel(Str $suffix) {
  given $suffix {
    when /^ (j? $VOWELS) $/ { return 'V', ~$0; }
    when /^ (j? $VOWELS) s $/ {
      if $0 eq 'o' | 'ô' { return 'OS', ~$0; }
      else { return 'AS', ~$0; }
    }
    when /^ (j? $VOWELS) (<[mnŋ]>) $/ {
      if $0 eq 'o' | 'ô' { return 'ON', ~$0, ~$1; }
      else { return 'AN', ~$0, ~$1; }
    }
    when /^ (j? $VOWELS) r $/ {
      if $0 eq 'o' | 'ô' { return 'OR', ~$0; }
      else { return 'AR', ~$0; }
    }
    when /^ (j? $VOWELS) l $/ {
      if $0 eq 'e' | 'ê' { return 'EL', ~$0; }
      else { return 'IL', ~$0; }
    }
    when /^ (j? $VOWELS) þ $/ { return 'Þ', ~$0; }
    when /^ (j? $VOWELS) rþ $/ { return 'RÞ', ~$0; }
  }
  die "$suffix is not a valid Ŋarâþ Crîþ suffix";
}

# Get the stems of the principal parts of a noun given its stem and suffix,
# as well as a dictionary.
sub get-principal-parts(Str $stem, Str $suffix, %entries, Str $class, Str $thv) {
  # All nouns have N, S and L forms.
  my %pp; # Hee hee
  my %noun-entry = %entries{$stem ~ $suffix}<tags> // %();
  my $stripj = ?($class eq 'OS' | 'ON' | 'OR' | 'EL');
  %pp<n> = $stem;
  if %noun-entry<l>:exists {
    # Noun classes for which the suffix is one syllable:
    # V AN AR IL OS OR ON EL
    # Noun classes for which the suffix is two syllables:
    # AS Þ RÞ
    my ($lstem, $lsuffix) = $class eq 'AS' | 'Þ' | 'RÞ' ??
      get-stem-and-suffix-twice(%noun-entry<l>, :$stripj) !!
      get-stem-and-suffix(%noun-entry<l>, :$stripj);
    %pp<l> = $lstem;
  } else {
    # Derive L stem: change last vowel
    my $new-stem = $stem.subst(/j? $VOWELS/, {
      transform-themvowel(%THEMATIC_VOWEL_DERIVATIVES_V, ~$/)[0]
    }, :nth(*));
    %pp<l> = $new-stem;
  }
  if %noun-entry<s>:exists {
    # The suffix is exactly one syllable.
    my ($sstem, $ssuffix) = get-stem-and-suffix(%noun-entry<s>, :$stripj);
    %pp<s> = $sstem;
  } else {
    my ($prefix, $vplusbridge) = get-stem-and-suffix($stem, :stripj, :!fatal);
    my $vowel = $vplusbridge ?? $vplusbridge.substr(0, 1) !! '';
    my $bridge = $vplusbridge ?? $vplusbridge.substr(1) !! $prefix;
    $prefix = '' if !$vplusbridge;
    $bridge = 'd' if $bridge eq 't' | 'st' | 's';
    $bridge = 'ð' if $bridge eq 'þ' | 'lþ' | 'rþ';
    $vplusbridge = $vowel ~ $bridge;
    $vplusbridge = $vplusbridge.subst(/ <!after <[aoâô]>> r /, 'l');
    $vplusbridge = $vplusbridge.subst(/ r $/, 'l');
    %pp<s> = $prefix ~ $vplusbridge;
  }
  if $stripj {
    # These classes also have the I form
    if %noun-entry<i>:exists {
      # The suffix is exactly one syllable.
      my ($istem, $isuffix) = get-stem-and-suffix(%noun-entry<i>, :stripj);
      die 'to?!!' if !$istem.starts-with('cja');
      %pp<i> = $istem.substr(3);
    } else {
      # Voice initial consonant
      if has-pf-onset($stem) {
        %pp<i> = $stem.subst(/^ . ** 2/, { $/.comb.map({%VOICED_CONSONANTS{$_} // $_}).join });
      } else {
        %pp<i> = $stem.subst(/^ ./, { %VOICED_CONSONANTS{~$/} // ~$/ });
      }
    }
    # Also add the lenited I form
    %pp<j> = lenite %pp<i>;
  }
  if $class eq 'EL' {
    # This class also has A and G forms
    if %noun-entry<a>:exists {
      # The suffix is exactly one syllable.
      my ($astem, $asuffix) = get-stem-and-suffix(%noun-entry<a>, :stripj);
      %pp<a> = $astem;
    } else {
      # Toggle palatalisation in the first syllable
      %pp<a> = toggle-j($stem);
    }
    if %noun-entry<g>:exists {
      # The suffix is exactly one syllable.
      my ($gstem, $gsuffix) = get-stem-and-suffix(%noun-entry<g>, :stripj);
      %pp<g> = $gstem;
    } else {
      # See 3.4.11 for details
      my $gstem = $stem.subst(/j? $VOWELS/, {
        transform-themvowel(%THEMATIC_VOWEL_DERIVATIVES_AN, ~$/)[0]
      }, :nth(*));
      $gstem ~= 'j' if !$gstem.contains('j');
      %pp<g> = $gstem;
    }
  }
  return %pp;
}

sub get-gender(Str $word, %entries, Str $class) {
  my $e = %entries{$word};
  if $e.defined && $e<pos>.starts-with('n') {
    return $e<pos>.substr(1);
  }
  # Infer from class
  note "Gender unspecified and word not listed in dictionary; inferring from suffix";
  return ($class eq 'OS' | 'ON' | 'OR' | 'EL') ?? "t" !! "c";
}

sub stringify-gender(Str $g) {
  return "UNKNOWN" if !$g.defined;
  return (%(<c celestial t terrestrial h human>)){$g} // "UNKNOWN";
}

sub decline-core-monosyllabic($gender, $noun, Str $pnoun) {
  @MONOSYLLABIC-OVERRIDES.map: {
    $pnoun ~
      .subst('O', $gender eq 't' ?? 'o' !! 'a')
      .subst('Ö', $gender eq 't' ?? 'o' !! <e a i>[letter-sum($noun) % 3])
  };
}

sub decline(Str $noun, Str $pnoun, %pp, $class, $thv, $thn, $markers, $gender) {
  my @declensions = @(%DECLENSIONS{$class});
  # special case
  if $class eq 'EL' && %pp<g>.comb.tail ~~ $VOWELS {
    @declensions[11] = "Gš0l";
  }
  my $monosyllabic = !is-multisyllabic($noun);
  if $monosyllabic {
    @declensions = @declensions.tail(* - 12)
  }
  my @tab = do given $class {
    when 'V' | 'Þ' | 'RÞ' {
      my @derivatives = 
        transform-themvowel(%THEMATIC_VOWEL_DERIVATIVES_V, $thv);
      @declensions.map: {
        .subst(/<[NLS]>/, { %pp{~$/.lc} }, :global)
        .subst('0', $thv, :global)
        .subst(/<[1..3]>/, { @derivatives[+$/ - 1] }, :global)
      }
    }
    when 'AS' | 'AR' {
      my @derivatives = 
        transform-themvowel(%THEMATIC_VOWEL_DERIVATIVES_AS, $thv);
      @declensions.map: {
        .subst(/<[NLS]>/, { %pp{~$/.lc} }, :global)
        .subst(/(<[0..3]>) "'"/, { @derivatives[2 * +$0] }, :global)
        .subst('0', $thv, :global)
        .subst(/<[1..3]>/, { @derivatives[2 * +$/ - 1] }, :global)
      }
    }
    when 'AN' {
      my @derivatives = 
        transform-themvowel(%THEMATIC_VOWEL_DERIVATIVES_AN, $thv);
      @declensions.map: {
        .subst(/<[NLS]>/, { %pp{~$/.lc} }, :global)
        .subst(/(<[1..3]>) "'"/, { @derivatives[2 * +$0 - 1] }, :global)
        .subst('0', $thv, :global)
        .subst(/<[1..3]>/, { @derivatives[2 * +$/ - 2] }, :global)
        .subst('ñ', $thn, :global)
      }
    }
    when 'IL' {
      my @derivatives = 
        transform-themvowel(%THEMATIC_VOWEL_DERIVATIVES_IL, $thv);
      @declensions.map: {
        .subst(/<[NLS]>/, { %pp{~$/.lc} }, :global)
        .subst(/[0\*|1|1\'|1\*|2|2\'|3\'|3\*]/, {
          my $i = %(<0* 1 1' 1* 2 2' 3' 3*>.antipairs){~$/};
          @derivatives[$i]
        }, :global)
        .subst('0', $thv, :global)
      }
    }
    when 'OS' | 'OR' | 'ON' | 'EL' {
      my $other = %(<e o ê ô o e ô ê>){$thv};
      @declensions.map: {
        .subst(/<[NAGLIJS]>/, { %pp{~$/.lc} }, :global)
        .subst('0', $thv, :global)
        .subst('1', $other, :global)
        .subst('ñ', $thn, :global)
      }
    }
    default {
      die "Class $class NYI";
    }
  };
  if $monosyllabic {
    @tab = flat decline-core-monosyllabic($gender, $noun, $pnoun), @tab;
  }
  @tab = @tab.map: {
    .subst('ji', 'i', :global);
  };
  if @tab[0].ends-with("aneliþ") {
    @tab[12] = @tab[12].subst(/ aniþas $ /, "anolðas");
  }
  @tab.kv.map: -> $index, $str {
    (is-multisyllabic($str) || $str eq @tab[$SINGULAR-GENITIVE]) &&
      $index == $ECLIPSED-IF-MULTISYLLABIC ??
      eclipse($str, $markers) !! ($markers ~ $str);
  };
}

# We use the number of graphemes as a proxy for string width for simplicity,
# but in principle, this could be adapted to use something like
# Terminal::WCWidth.
sub pad-left($str, $len) {
  $str ~ (' ' x ($len - $str.chars));
}

sub print-table(@data) {
  my $output = "\n ";
  my $cols = @data.map(+*).max;
  my @widths = (^$cols).map({ @data.map(*[$_].chars).max });
  for @data -> @row {
    for @row.pairs -> $p {
      $output ~= pad-left($p.value, @widths[$p.key]);
      $output ~= ' ';
    }
    $output ~= "\n\n ";
  }
  say $output;
}

sub print-declension-table(@tab) {
  my @data = $(["Case \\ Num", "Singular", "Dual", "Plural"]);
  for ^NCASES -> $i {
    my @row = @CASE_NAMES[$i];
    for ^NNUMBERS -> $j {
      @row.push(@tab[NNUMBERS * $i + $j]);
    }
    @data.push($@row);
  }
  print-table(@data);
}

my %*SUB-MAIN-OPTS =
  :named-anywhere, # Free word order FTW!
  ;

sub MAIN(
    Str $noun, #= a noun to decline
    Str :$gender is copy, #= override for the gender of the noun
    Str :$mode = "traditional" #= use 'traditional' or 'realigned' gender
    ) {
  my ($markers, $pnoun) = extract-marker $noun;
  my %entries = read-dictionary("4v7/dict/main.dict".IO.open, $mode eq "realigned");
  my ($stem, $suffix) = get-stem-and-suffix($pnoun);
  if $suffix ~~ /^ j ([ <[oô]> <[smnŋr]> ] | [ <[eê]> l ]) $/ {
    $stem ~= 'j';
    $suffix = ~$0;
  }
  my ($class, $thv, $thn) = get-suffix-class-and-thematic-vowel($suffix);
  my %pp = get-principal-parts($stem, $suffix, %entries, $class, $thv);
  my $osnouns = so $class eq 'OS' | 'ON' | 'OR' | 'EL';
  if !$gender.defined {
    $gender = get-gender($noun, %entries, $class);
  }
  say "Stem: $stem";
  say "Suffix: $suffix";
  say "Markers: {$markers // "none"}";
  say "Class: $class";
  say "Thematic vowel: $thv";
  say "Thematic nasal: $thn" if $thn.defined;
  say "Gender: {stringify-gender($gender)}";
  say "Principal stems:";
  say "  N: {%pp<n>}-";
  say "  A: {%pp<a>}-" if $class eq 'EL';
  say "  G: {%pp<g>}-" if $class eq 'EL';
  say "  L: {%pp<l>}-";
  say "  I: -{%pp<i>}-" if $osnouns;
  say "  S: {%pp<s>}-";
  my @tab = decline($noun, $pnoun, %pp, $class, $thv, $thn, $markers, $gender);
  print-declension-table(@tab);
}
