use strict;
use fatal;

constant $YEAR-LEN = 403 + 32 / 139;
constant $PERIOD = 139;

my $days-past = 0;
for ^$PERIOD -> $year {
  my @malenvo = $YEAR-LEN «*« ($year «+« (^9) »/» 8);
  my @rounded-malenvo = @malenvo.map(*.floor);
  my @days-of-year = @rounded-malenvo »-» $days-past;
  printf "%3d ", $year;
  for ^4 {
    print "{@days-of-year[2 * $_] - @days-of-year[2 * $_ + 1]}..";
    print "{@days-of-year[2 * $_ + 2] - @days-of-year[2 * $_ + 1] - 1} ";
  }
  say @days-of-year[8];
  $days-past = floor($YEAR-LEN * ($year + 1));
}
