Someone previously mentioned using 黒ン語 for 'clongo', so I had an idea:

> if clongo is black language  
> then the three main types thereöf should be called cyan, magenta and yellow languages

So here they are:

* Since auxlangers are cowards, auxlangs are 黄ん語 (*cingo*, 'yellow language').
* Cyan reminds me of techy stuff, so engelangs are シアン語 (*sïango*, 'cyan language').
* Artlangs get the remaining primary colour, so they are called 紫ん語 (*mwrasacingo*, 'purple language').
* Aux-engelang hybrids are 緑ん語 (*midoringo*, 'green language').
* Art-engelang hybrids such as ŋarâþ crîþ v7 are called 青ん語 (*aongo*, 'blue language').
* Art-auxlang hybrids are called 赤ん語 (*acango*, 'red language').
* Bad conlangs are called 灰色ん語 (*haïirongo*, 'grey language').
* Relexes are called 銀語 (*gingo*, 'silver language'); cyphers are called もっと銀語 (*motto-gingo*, 'more silver language').
* Natlangs, of course, are 白ん語 (*sirongo*, 'white language').
