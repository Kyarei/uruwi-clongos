use strict;
use fatal;

sub MAIN(Str $s) {
  my $a;
  my $stem;
  if $s ~~ / ^ (.*) 'eat' $ / {
    $stem = $0;
    $a := <ea exes exe ean exesen exen exeθ exeɹ eiː>;
  } elsif $s ~~ / ^ (.*) 'ait' $ / {
    $stem = $0;
    $a := <ae axas axa aen aesan axan axaθ axaɹ ao>;
  } elsif $s ~~ / ^ (.*) 'at' $ / {
    $stem = $0;
    $a := <a es e an esen en eθ eɹ iː>;
  } elsif $s ~~ / ^ (.*) 'it' $ / {
    $stem = $0;
    $a := <e as a en esan an aθ aɹ o>;
  } else {
    die 'Fuck!';
  }
  say $s;
  for @($a) {
    say $stem ~ $_;
  }
  say $s ~ 'it';
  for @($a) {
    say $stem ~ $_ ~ 'ta';
  }
}
