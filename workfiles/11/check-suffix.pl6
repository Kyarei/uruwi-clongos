#!/usr/bin/env perl6

use fatal;
use Terminal::ANSIColor;

constant @LETTERS = <
  m n ŋ p b t d c g f s š h ł r l i u e o a
>;

constant %SUCCESSORS = (
  'm' => <b f i u e o a>,
  'n' => <d s š l i u o a $>,
  'ŋ' => <g u e o a $>,
  'p' => <f r l i e a>,
  'b' => <f r l i e a>,
  't' => <s š ł r l u e a>,
  'd' => <s š ł r l u e a>,
  'c' => <f s ł r l u e o a $>,
  'g' => <f s ł r l u e o a $>,
  'f' => <i e a $>,
  's' => <t d l u e o a $>,
  'š' => <i e o>,
  'h' => <i e a $>,
  'ł' => <p t c e o a $>,
  'r' => <i u e o a>,
  'l' => <p t d c g i u e a $>,
  'i' => <n ŋ b d g s l i $>,
  'u' => <ŋ s l e $>,
  'e' => <p b t d c g f s š h ł l i a $>,
  'o' => <p t c f s r u e a $>,
  'a' => <p b t d c g s š h ł r l i u e o>,
).map({ .key => set .value });

constant @AFFIX-COLOURS =
  200, 33, 121, 214, 123, 154, 183, 87
  ;

sub colour-affix(Str $c, Int $i) {
  colored($c, ~@AFFIX-COLOURS[$i mod +@AFFIX-COLOURS]);
}

sub validate(Str $s) {
  for ($s ~ '$').comb.rotor(2 => -1) {
    if not %SUCCESSORS{$_[0]}{$_[1]} {
      return ('forbidden', $_[0], $_[1]);
    }
  }
  ()
}

sub check-suffix-exhaustion(@a) {
  @LETTERS.map(sub ($l) {
    my $suffix-index = Any;
    for @a.kv -> $i, $s {
      my @res = validate $l ~ $s;
      if not @res {
        $suffix-index = $i;
        last;
      }
    }
    $suffix-index
  });
}

# I don't know how to do cmdline varargs from MAIN -- quick and dirty hack
my @valid-suffices = @*ARGS.grep(sub ($s) {
  my $stat = validate $s;
  if $stat {
    say BOLD, color('red'),
      "Forbidden cluster in ",
      BOLD_OFF, ITALIC, color('yellow'),
      "〈$s〉",
      ITALIC_OFF, BOLD, color('red'),
      ": ",
      BOLD_OFF, ITALIC, color('yellow'),
      "〈{$stat[1]}{$stat[2]}〉", RESET;
  }
  not $stat;
});

say BOLD, color('magenta'),
  "Valid suffices: ",
  BOLD_OFF, ITALIC, color('yellow'),
  "〈",
  @valid-suffices.pairs.map({ colour-affix(.value, .key) }).join(" "),
  ITALIC, color('yellow'),
  "〉",
  RESET;

my @chosen-suffices = check-suffix-exhaustion @valid-suffices;
for @chosen-suffices.kv -> $i, $j {
  if not $j.defined {
    say BOLD, color('red'),
      "No suffix valid after ",
      BOLD_OFF, ITALIC, color('yellow'),
      "〈{@LETTERS[$i]}〉", RESET;
  } else {
    my $c = @valid-suffices[$j];
    say ITALIC,
      "...",
      ITALIC_OFF, BOLD, color('green'),
      "{@LETTERS[$i]}",
      BOLD_OFF, color('reset'),
      "-",
      colour-affix($c, $j),
      RESET;
  }
}
