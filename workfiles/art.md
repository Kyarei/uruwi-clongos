# Is Conlanging an Art?

A script for a future video.

Summary: Is conlanging an art? If it is, then why is it not appreciated as an art very much?

A few days ago, /u/cabrowritter posted a question to /r/conlangs: [Is conlanging an art?][thread] The concept of conlangs as an art form is not new: [a post on a mailing list for conlangs from 2002][mailinglist2002] has called for conlangs to be appreciated as an art form, and there has been [at least one book][aoli] whose title implies that this is the case. When I work on conlangs, I also consider my works to be art.

(explore definition of art)

Debating whether conlangs are art requires us to define what art is. The word 'art' has often been used broadly to refer to any type of skill, [especially before the 17th century][wp-art], but when we ask 'is conlanging an art?', we are referring to a narrower definition.

(traits of art)

Before we full-on jump into a dictionary definition of the term, let's look at some of the traits of art. That is, what abstract qualities come into mind when we hear 'art'? The first trait we might imagine is the use of skill, as in the original sense of the word. Many people consider a work of art to be 'good' if it shows a lot of skill from the creator. I've definitely seen a drive for skill is present in the conlanging community, and many conlangers have role models they look up to for their skills in language invention.

Another definiting trait is the use of creativity or novelty – in contrast to crafts, there is some degree of originality expected in an artistic work. Not all conlangs are equally creative: auxlangs such as Esperanto justifiably avoid exotic features, but creativity is encouraged in other types of conlangs. More on this later.

Finally, art has the goal of aesthetics in mind, even when that sense of aesthetics is unconventional. In other words, it serves a purpose beyond the utilitarian. This, too, is a trait that a sizeable fraction of conlangs exhibit.

If we check definitions of 'art' from dictionaries (links below), we can see that they identify these traits as well.

(note: not every conlang is art – cf. functional vs. aesthetic works in other mediums)

It's important to note that the notion that conlanging is an art doesn't imply that *all* conlangs are art. An auxlang, for example, is made to help people with different native langauges communicate with each other. Making one certainly requires skill, but creativity and aesthetics are non-goals; therefore, I'd consider constructing auxlangs to be a craft or a science, but not art. /u/Quel2324 provides an analogy with digital drawings: both a drawing from an IKEA manual and [this drawing][faces] are digital drawings, but one wouldn't normally be considered art while the other is.

(possible objections:)

Given this definition of art, I can say that I consider conlanging as an art, or at least that it can be an art form. So why isn't conlanging widely recognised as an art form of its own?

(novelty)

An obvious reason is its novelty. Serious language creation rose only a century or two ago, and while auxiliary languages had some public support, artlangs remained a niche field until the rise of Internet communities. There might still be stigma around conlanging, especially for artistic purposes.

(hard to appreciate by laymen?)

(opposition from conlangers who see it as a 'hobby')

(comparison to 'are video games art?')


definitions: [lexico][lexico] [mw][mw]

[thread]: https://www.reddit.com/r/conlangs/comments/f3ase4/is_conlanging_an_art/
[mailinglist2002]: http://archives.conlang.info/fo/jinbua/vhavheintian.html
[aoli]: http://www.artoflanguageinvention.com/books/
[wp-art]: https://en.wikipedia.org/wiki/Art
[faces]: https://www.salleurl.edu/sites/default/files/styles/ample_1400/public/content/nodes/Estudio/image/16205/25297/grado-en-artes-digitales3.jpg
[lexico]: https://www.lexico.com/definition/art
[mw]: https://www.merriam-webster.com/dictionary/art
